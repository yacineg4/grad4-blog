import React from 'react'
import { Link } from 'gatsby'

import logo from '../img/logo_white.svg'
import facebook from '../img/social/facebook.svg'
import linkedin from '../img/social/linkedin.svg'

import './footer.sass'

import MailchimpSubscribe from "react-mailchimp-subscribe"

const url = "https://grad4.us3.list-manage.com/subscribe/post?u=c2334f5438c33a236ec6ef78d&amp;id=55b3739039";

// simplest form (only email)
const SimpleForm = () => <MailchimpSubscribe url={url}/>

const Footer = class extends React.Component {
  render() {
    return (
      <footer className="footer has-text-white-ter">
        <div className="content has-text-centered has-text-white-ter">
          <div className="container has-text-white-ter">
            <div className="columns">
              <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li>
                      <Link to="/" className="navbar-item">
                        Confidentialité
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/about">
                        Politique et modalités
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/products">
                        Pour nous joindre
                      </Link>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section>
                  <div className="content has-text-centered">
                    <img
                      src={logo}
                      alt="Kaldi"
                      style={{ width: '20em', height: '14em' }}
                    />
                  </div>
                </section>
              </div>
              <div className="column is-4 social">
              <p class="reseaux">Suivez-nous sur les réseaux sociaux</p>
              <a title="twitter" href="https://twitter.com">
                  <img
                    className="fas fa-lg"
                    src={linkedin}
                    alt="Linkedin"
                    style={{ width: '1em', height: '1em' }}
                  />
                </a>
                <a title="facebook" href="https://facebook.com">
                  <img
                    src={facebook}
                    alt="Facebook"
                    style={{ width: '1em', height: '1em' }}
                  />
                </a>
              </div>
            </div>
            <div className="columns">
              <div className="column">
                <SimpleForm/>
              </div>
            </div>
            <div className="columns">
              <div className="column">
                <p id="copyright">© GRAD4 2019 - Tous droits réservés.</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
