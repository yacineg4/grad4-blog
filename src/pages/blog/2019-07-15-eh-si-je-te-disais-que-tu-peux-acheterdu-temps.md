---
templateKey: blog-post
title: EH SI JE TE DISAIS QUE TU PEUX ACHETERDU TEMPS?
date: 2019-07-15T17:16:18.451Z
description: Tout en  étant avantagé de la situation.
featuredpost: false
featuredimage: /img/mech_image_1.jpg
tags:
  - '4.0'
  - automatisation
  - grad4
---
Trop souvent dans nos vies professionnelles et même personnelles on se dit que ça serait tellement bien d’acheter du temps. Si on pouvait éliminer les tâches ardues qu’on n’aime pas faire et se concentrer sur ce qui est le ‘fun’, la vie serait tellement meilleure. Dans cette série de blogues, découvrez com-ment acheter du temps!

Commençons par l’aspect professionnel manufacturier. Comment acheter du temps. J’imagine que vous avez un doute, eh bien oui, la robotisationet l’automatisation!

On peut donc acheter du temps pour nos employés sur le plancher en mettant en place un robot pour la palettisation, le chargement de machines CNC, la peinture de pièces, la découpe de fibre de verre ou de métaux, l’application de colle ou de scellant et plus encore!

![Someone working](/img/mech_image_2.jpg)

Par contre, ce n’est pas si simple, on doit se poser les bonnes questions, investir de l’argent et surtout investir du temps afin d’implanter vos solutions robotiques et d’automatisation dans votre usine. Ne pensez pas que ceci se fera en une journée et que le tour sera joué.

C’est vraiment drôle à dire, mais quand on veut sauver du temps grâce à des so-lutions robotiques ou d’automatisation, le plus gros bloqueur au déploiement de ce type de technologie est le temps! Eh bien oui, pour sauver du temps on doit investir du temps! Trop souvent les gens mettent de côté ce type de projet à cause des urgences quotidiennes sur le plancher de production. Dans le court terme, on garde le cap, mais la ligne d’arrivée sera de plus en plus loin et vous serez dépassé avant d’y arriver.

Contactez-nous, vos experts, pour vous faire sauver du temps dans vos projets robots et d’automatisation.
